package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/ariefperdiansyah/go-say-hello/v2/entity"
	"gitlab.com/ariefperdiansyah/go-say-hello/v2/repository"
)

var categoryRepository = &repository.CategoryRepositoryMock{Mock: mock.Mock{}}
var categoryService = CategoryService{Rep: categoryRepository}

func TestCategoryService_GetFail(t *testing.T) {
	//program mock
	categoryRepository.Mock.On("FindById", "1").Return(nil)
	category, err := categoryService.Get("1")
	assert.Nil(t, category)
	assert.NotNil(t, err)
}
func TestCategoryService_GetSucess(t *testing.T) {

	categoryRet := entity.Category{
		Id:   "1",
		Name: "Laptop",
	}
	//program mock
	categoryRepository.Mock.On("FindById", "2").Return(categoryRet)
	category, err := categoryService.Get("2")
	assert.Nil(t, err)
	assert.NotNil(t, category)
	assert.Equal(t, categoryRet.Id, category.Id)
}
