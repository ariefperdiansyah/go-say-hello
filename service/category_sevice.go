package service

import (
	"errors"

	"gitlab.com/ariefperdiansyah/go-say-hello/v2/entity"
	"gitlab.com/ariefperdiansyah/go-say-hello/v2/repository"
)

type CategoryService struct {
	Rep repository.CategoryRepository
}

func (serv CategoryService) Get(id string) (*entity.Category, error) {
	category := serv.Rep.FindById(id)
	if category == nil {
		return category, errors.New("category not found")
	} else {
		return category, nil
	}
}
