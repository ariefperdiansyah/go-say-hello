package repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/ariefperdiansyah/go-say-hello/v2/entity"
)

type CategoryRepositoryMock struct {
	Mock mock.Mock
}

func (rep *CategoryRepositoryMock) FindById(id string) *entity.Category {
	arguments := rep.Mock.Called(id)
	if arguments.Get(0) == nil {
		return nil
	} else {
		category := arguments.Get(0).(entity.Category)
		return &category
	}
}
