package repository

import (
	"gitlab.com/ariefperdiansyah/go-say-hello/v2/entity"
)

type CategoryRepository interface {
	FindById(Id string) *entity.Category
}
