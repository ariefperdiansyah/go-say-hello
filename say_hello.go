package go_say_hello

func SayHello() string {
	return "Hello World"
}

func SayHelloWithName(name string) string {
	return "Hello " + name
}
