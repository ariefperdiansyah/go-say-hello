package helper

import (
	"fmt"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

//will run in every test
func TestMain(m *testing.M) {
	fmt.Println("sebelum test")
	m.Run() //execute all test
	fmt.Println("setelah test")
}

//go test -v -run TestHelloWorld
//go test -v -> all function inside folder
//go test -v ./... -> running in root and all package/folder
func TestHelloWorld(t *testing.T) {
	result := HelloWorld("Eko")
	if result != "Hello Eko" {
		// unit test failed
		t.Fail() // will still execute code under this function
	}

	fmt.Println("After logic1")
}

func TestHelloWorld2(t *testing.T) {
	result := HelloWorld("Budi")
	if result != "Hello Budi" {
		// unit test failed
		t.FailNow() //will not execute code under this function
	}
	fmt.Println("After logic2")

}

func TestHelloWorld3(t *testing.T) {
	result := HelloWorld("Budi")
	if result != "Hello Budi" {
		// unit test failed
		t.Error("not match") // return error and call t.Fail
	}
	fmt.Println("After logic3")

}

func TestHelloWorld4(t *testing.T) {
	result := HelloWorld("Budi")
	if result != "Hello Budi" {
		// unit test failed
		t.Fatal("not match") // return error and call t.FailNow
	}
	fmt.Println("After logic4")

}

func TestHelloWorldAssertion(t *testing.T) {
	result := HelloWorld("Chalie")
	assert.Equal(t, "Hello Chalie", result, "Result not match") //assert if fail, will call fail
	fmt.Println("after test with assert")
}

func TestHelloWorldRequire(t *testing.T) {
	result := HelloWorld("Chalie")
	require.Equal(t, "Hello Chalie", result, "Result not match") //require if fail, will call failNow
	fmt.Println("after test with require")
}

func TestHelloWorldSkip(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("Can not run on Mac OS") //if skip, will auto pass
	}
	result := HelloWorld("Chalie")
	require.Equal(t, "Hello Chalie", result, "Result not match") //require if fail, will call failNow
}

//run subtest only
// go test -run {nama test function}/{namasubtest}
// go test -run TestSubTest/checkOS
func TestSubTest(t *testing.T) {
	t.Run("checkOS", func(t *testing.T) {
		if runtime.GOOS == "windows" {
			t.Skip("Can not run on Mac OS") //if skip, will auto pass
		}
		result := HelloWorld("Chalie")
		require.Equal(t, "Hello Chalie", result, "Result not match") //require if fail, will call failNow
	})
	t.Run("checkName", func(t *testing.T) {
		result := HelloWorld("Chalie")
		assert.Equal(t, "Hello Chalie", result, "Result not match") //assert if fail, will call fail
		fmt.Println("after test with assert")
	})
}

//run test table (looping)
func TestHelloTable(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "Eko",
			request:  "Eko",
			expected: "Hello Eko",
		},
		{
			name:     "Budi",
			request:  "Budi",
			expected: "Hello Budi",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			results := HelloWorld(test.request)
			require.Equal(t, test.expected, results)
		})
	}
}

//benchmark function
// want to all with test go test -v -bench=.
// want to test only all benchmark go test -v -run=testtidakada -bench=.
// want specific benchmark go test -v -run=testtidakada -bench=BenchmarkHelloWorldBudi
func BenchmarkHelloWorld(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Eko")
	}
}
func BenchmarkHelloWorldBudi(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Budi")
	}
}

//sub benchmark
// want specific benchmark go test -v -run=testtidakada -bench=BenchmarkSub/Kurniawan
func BenchmarkSub(b *testing.B) {
	b.Run("Eko", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Eko")
		}
	})
	b.Run("Kurniawan", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Kurniawan")
		}
	})
}

//benchmark table
func BenchmarkTable(b *testing.B) {
	benchmarks := []struct {
		name    string
		request string
	}{
		{
			name:    "Eko",
			request: "Eko",
		},
		{
			name:    "Budi",
			request: "Budi",
		},
	}

	for _, benchmark := range benchmarks {
		b.Run(benchmark.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				HelloWorld(benchmark.request)
			}
		})
	}
}
